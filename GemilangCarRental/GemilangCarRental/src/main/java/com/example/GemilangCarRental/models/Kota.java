package com.example.GemilangCarRental.models;

import jakarta.persistence.*;

@Entity
@Table(name = "kota")
public class Kota extends CommenEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name= "nama_kota", nullable = false)
    private String namaKota;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNamaKota() {
        return namaKota;
    }

    public void setNamaKota(String namaKota) {
        this.namaKota = namaKota;
    }
}
