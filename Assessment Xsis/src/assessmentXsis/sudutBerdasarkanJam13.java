package assessmentXsis;

import java.text.DecimalFormat;
import java.util.Scanner;

public class sudutBerdasarkanJam13 {

    public static void nomor13(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan jam = ");
        String inputJam = input.nextLine();

        String[] splitInput =inputJam.split(":");
        double jam = Double.parseDouble(splitInput[0]);
        double menit = Double.parseDouble(splitInput[1]);
        double arahMenit = (menit/60) * 12;
        double nilaiSudutPerJam = 30;
        double output = 0;

        if (menit > 0){
            jam += (menit/60);
        }
        if (jam >= 12){
            jam -= 12;
        }
        if (jam > arahMenit){
            output = (jam - arahMenit) * nilaiSudutPerJam;
        } else if (jam < arahMenit) {
            output = (arahMenit - jam) * nilaiSudutPerJam;
        }

        if (output > 180){
            output = 360 - output;
        }

        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Output = " + df.format(output) + " derajat");

    }

}
