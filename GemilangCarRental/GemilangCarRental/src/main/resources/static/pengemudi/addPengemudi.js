$("#editCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#editCreateBtn").click(function(){
	var pegawai = $("#nomInput").val();
	var pengemudi = $("#namaInput").val();

	if(pegawai == ""){
		$("#errNom").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNom").text("");
	}
	if(pengemudi == ""){
    	$("#errNama").text("Initial tidak boleh kosong!");
    		return;
    } else {
    	$("#errNama").text("");
    }

	var obj = {};
	obj.noPegawai = pegawai;
	obj.namaPengemudi = pengemudi;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addPengemudi",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})