package com.example.GemilangCarRental.controllerUI;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("armada")
public class ArmadaController {

    @RequestMapping("")
    public String armada(){
        return ("armada/Armada");
    }

    @RequestMapping("addArmada")
    public String addarmada(){ return "armada/addArmada";}

    @RequestMapping("editArmada/{id}")
    public String editArmada(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "armada/editArmada";
    }
    @RequestMapping("deleteArmada/{id}")
    public String deleteArmada(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "armada/deleteArmada";
    }

}
