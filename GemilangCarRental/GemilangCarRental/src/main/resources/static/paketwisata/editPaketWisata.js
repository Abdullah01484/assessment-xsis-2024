$(document).ready(function() {
	getPaketWisataById();

})

var createdOn;
var createdBy;

$(document).ready(function(){
   pilihanOpsi();
})

function pilihanOpsi(){
    var harga = document.getElementById("opsiKota");

    $.ajax({
            url : "/api/getAllKota",
            type : "GET",
            contentType : "application/json",
            success : function(data){
                for(i = 0; i < data.length; i++){
                    harga.options[harga.options.length] = new Option(data[i].namaKota, data[i].id);
                }
            }
    });
}

function getPaketWisataById() {
	var id = $("#editPaketWisataId").val();
	$.ajax({
		url: "/api/getByIDPaketWisata/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_by;
            createdOn = data.created_on;
			$("#opsiKota").val(data.kotaId);
            $("#namaInput").val(data.namaWisata);
            $("#kapasitasInput").val(data.kapasitas);
            $("#hargaInput").val(data.harga);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idpaketwisata = $("#editPaketWisataId").val();
	var kota = $("#opsiKota").val();
	var wisata = $("#namaInput").val();
	var kapasitas = $("#kapasitasInput").val();
	var harga = $("#hargaInput").val();

	if (kota == "") {
    	$("#errKota").text("Name tidak boleh kosong!");
    	return;
    } else {
    	$("#errKota").text("");
    }
	if (wisata == "") {
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}
	if (kapasitas == "") {
        $("#errKapasitas").text("Name tidak boleh kosong!");
        return;
    } else {
        $("#errKapasitas").text("");
    }
    if (harga == "") {
        $("#errHarga").text("Name tidak boleh kosong!");
        return;
    } else {
        $("#errHarga").text("");
    }

	var obj = {};
	obj.id = idpaketwisata;
	obj.kotaId = kota;
	obj.namaWisata = wisata;
	obj.kapasitas = kapasitas;
	obj.harga = harga;

	var myJson = JSON.stringify(obj);
	console.log(obj);

	$.ajax({
		url: "/api/editPaketWisata/" + idpaketwisata,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
			$(".modal").modal("hide")
			location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
