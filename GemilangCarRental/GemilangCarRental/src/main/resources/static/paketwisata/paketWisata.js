function getAllPaketWisata(){
	$("#paketWisataTable").html(
		`<thead>
			<tr>
				<th>Id Paket Wisata</th>
				<th>Kota</th>
				<th>Nama Wisata</th>
				<th>Kapasitas</th>
				<th>Harga</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="paketWisataTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getAllPaketWisata",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#paketWisataTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${(data[i].kota).namaKota}</td>
						<td>${data[i].namaWisata}</td>
						<td>${data[i].kapasitas}</td>
                        <td>${data[i].harga}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editPaketWisata(this.value)" class="btn btn-warning">
                        	<i class="bi-pencil-square"> Edit</i>
                        	</button>
                        	<button value="${data[i].id}" onClick="deletePaketWisata(this.value)" class="btn btn-danger">
                            <i class="bi-trash"> Delete</i>
                            </button>
                        <td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/paket_wisata/addPaketWisata",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Paket Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editPaketWisata(id){
	$.ajax({
		url: "/paket_wisata/editPaketWisata/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Paket Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletePaketWisata(id){
	$.ajax({
		url: "/paket_wisata/deletePaketWisata/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Paket Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function PaketWisataList(currentPage, length) {
    $.ajax({
		url : '/api/paketwisatamapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {
			let table = '<select class="custom-select mt-3" id="size" onchange="PaketWisataList(0,this.value)">'
			table += '<option value="...">...</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>Id Paket Wisata</th> <th>Kota</th> <th>Nama Wisata</th> <th>Kapasitas</th> <th>Harga</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.paketwisata.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + (data.paketwisata[i].kota).namaKota + "</td>";
				table += "<td>" + data.paketwisata[i].namaWisata + "</td>";
				table += "<td>" + data.paketwisata[i].kapasitas + "</td>";
				table += "<td>" + data.paketwisata[i].harga + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.paketwisata[i].id + "' onclick=editPaketWisata(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.paketwisata[i].id + "' onclick=deletePaketWisata(this.value)>Delete</button></td>";
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="PaketWisataList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PaketWisataList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="PaketWisataList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#paketwisataList').html(table);
		}

	});
}

function SearchPaketWisata(request) {

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchpaketwisata/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>Id Paket Wisata</th> <th>Kota</th> <th>Nama Wisata</th> <th>Kapasitas</th> <th>Harga</th> <th>Action</th> </tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
                        table += "<td>" + result[i].id + "</td>";
                        table += "<td>" + result[i].kota.namaKota + "</td>";
                        table += "<td>" + result[i].namaWisata + "</td>";
                        table += "<td>" + result[i].kapasitas + "</td>";
                        table += "<td>" + result[i].harga + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editPaketWisata(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deletePaketWisata(this.value)>Delete</button></td>";
						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='6' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#paketwisataList').html(table);
			}
		});
	} else {
        PaketWisataList(0,5);
	}
}

$(document).ready(function(){
//	getAllPaketWisata();
    PaketWisataList(0,5);
})