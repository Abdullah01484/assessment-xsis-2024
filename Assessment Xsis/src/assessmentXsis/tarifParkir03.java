package assessmentXsis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class tarifParkir03 {
    public static void nomor03(){
        Scanner input = new Scanner(System.in);

        int hari = 0;
        int biayaparkir = 0;
        Date mp = null, kp = null;

        SimpleDateFormat waktu = new SimpleDateFormat("dd MMMM yyyy | HH:mm:ss", new Locale("id"));
        System.out.println("Masukkan Tanggal Parkir dan waktu parkirnya = ");
        String masukparkir = input.nextLine();

        System.out.println("Masukkan Tanggal Keluar dan waktu keluarnya = ");
        String keluarparkir = input.nextLine();

        try {
            mp = waktu.parse(masukparkir);
            kp = waktu.parse(keluarparkir);
        } catch (ParseException e) {
            System.out.println("format jam tidak valid");
        }

        long waktuparkir = kp.getTime() - mp.getTime();
        long convertJamParkir = waktuparkir / (1000 * 60 * 60);
        System.out.println("Waktu Parkir = " + convertJamParkir);

        if (convertJamParkir <= 8){
            biayaparkir = (int) convertJamParkir * 1000;
            System.out.println("Biaya Parkir = " +  biayaparkir);
        } else if (convertJamParkir > 8 && convertJamParkir <= 24) {
            biayaparkir = 8000;
            System.out.println("Biaya Parkir = " + biayaparkir);
        }else {
            hari = (int) convertJamParkir - 24;
            biayaparkir = (hari * 1000) + 15000;
            System.out.println("Biaya Parkir = " + biayaparkir);

        }
    }
}
