package assessmentXsis;

import java.util.Scanner;

public class batuGuntingKertas20 {

    public static void nomor20() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jarak awal : ");
        int jarak = input.nextInt();
        input.nextLine(); //bugged
        System.out.println("Masukkan data pemain A : ");
        String pemainA = input.nextLine().trim().toLowerCase();
        System.out.println("Masukkan data pemain B : ");
        String pemainB = input.nextLine().trim().toLowerCase();

        char[] pemainACharArray = pemainA.toCharArray();
        char[] pemainBCharArray = pemainB.toCharArray();

        while (pemainACharArray.length != pemainBCharArray.length) {
            System.out.println("Banyak data kedua pemain tidak sama!");
            System.out.println("Masukkan data pemain A : ");
            pemainA = input.nextLine().trim().toLowerCase();
            System.out.println("Masukkan data pemain B : ");
            pemainB = input.nextLine().trim().toLowerCase();

            pemainACharArray = pemainA.toCharArray();
            pemainBCharArray = pemainB.toCharArray();
        }

        String pemainAString = pemainA.replaceAll(" ", "");
        String pemainBString = pemainB.replaceAll(" ", "");

        char[] pemainACharFix = pemainAString.toCharArray();
        char[] pemainBCharFix = pemainBString.toCharArray();

        for (int i = 0; i < pemainACharFix.length; i++) {
            if (jarak > 0 && pemainACharFix[i] == 'g') {
                if (pemainBCharFix[i] == 'b') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain B Menang !");
                    }
                } else if (pemainBCharFix[i] == 'k') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain A Menang !");
                    }
                }
            } else if (jarak > 0 && pemainACharFix[i] == 'b') {
                if (pemainBCharFix[i] == 'k') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain B Menang !");
                    }
                } else if (pemainBCharFix[i] == 'g') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain A Menang !");
                    }
                }
            } else if (jarak > 0 && pemainACharFix[i] == 'k') {
                if (pemainBCharFix[i] == 'g') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain B Menang !");
                    }
                } else if (pemainBCharFix[i] == 'b') {
                    jarak -= 1;
                    if(jarak == 0){
                        System.out.println("Pemain A Menang !");
                    }
                }
            }
        }

        if (jarak > 0 ) {
            System.out.println("Draw");
        }

    }
}
