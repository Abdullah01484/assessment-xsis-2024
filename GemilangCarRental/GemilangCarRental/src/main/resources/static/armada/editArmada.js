$(document).ready(function() {
	getArmadaById();

})

var createdOn;
var createdBy;

function getArmadaById() {
	var id = $("#editArmadaId").val();
	$.ajax({
		url: "/api/getByIDArmada/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_by;
            createdOn = data.created_on;
			$("#kenInput").val(data.kendaraan);
			$("#harInput").val(data.harga);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
    var id = $("#editArmadaId").val();
	var jenismobil = $("#kenInput").val();
	var jumlah = $("#harInput").val();

	if (jenismobil == "") {
		$("#errKen").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKen").text("");
	}
	if (jumlah == "") {
    	$("#errHar").text("Initial tidak boleh kosong!");
    	return;
    } else {
    	$("#errHar").text("");
    }

	var obj = {};
	obj.created_by = createdBy;
    obj.id = id;
	obj.kendaraan = jenismobil;
	obj.harga = jumlah;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editArmada/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
			$(".modal").modal("hide")
			location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
