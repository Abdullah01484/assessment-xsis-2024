package assessmentXsis;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class minMax08 {

    public static void nomor08() {
        boolean flag = true;

        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Deret Angka (min 4) : ");

        String text = input.nextLine().trim();

        String[] textArray = text.split(" ");

        while (textArray.length < 4){
            System.out.println("Deret angka kurang dari 4 ");
            System.out.print("Tolong Masukkan Deret Angka : ");

            text = input.nextLine().trim();

            textArray = text.split(" ");
        }

        Integer[] intArray = new Integer[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray [i] = Integer.parseInt(textArray[i]);
        }

        Arrays.sort(intArray);
        int min = 0;
        for (int i = 0; i < 4; i++) {
            min += intArray[i];
        }

        Arrays.sort(intArray, Collections.reverseOrder());
        int max = 0;
        for (int i = 0; i < 4; i++) {
            max += intArray[i];
        }

        System.out.println("Nilai Minimal : " + min);
        System.out.println("Nilai Maximal : " + max);

    }
}
