package com.example.GemilangCarRental.repository;

import com.example.GemilangCarRental.models.Kota;
import com.example.GemilangCarRental.models.PaketWisata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoPaketWisata extends JpaRepository<PaketWisata, Long> {

    @Query(value = "SELECT * FROM paket_wisata ORDER BY id ASC", nativeQuery = true)
    List<PaketWisata> FindAllPaketWisataOrderByASC();

    @Query(value = "SELECT * FROM paket_wisata ORDER BY paket_wisata ASC", nativeQuery = true)
    Page<PaketWisata> FindAllPaketWisataOrderByASC(Pageable pageable);

    @Query(value = "SELECT * FROM paket_wisata WHERE lower(nama_wisata) LIKE lower(concat('%',?1,'%') ) ", nativeQuery = true)
    List<PaketWisata> SearchPaketWisata(String keyword);

}
