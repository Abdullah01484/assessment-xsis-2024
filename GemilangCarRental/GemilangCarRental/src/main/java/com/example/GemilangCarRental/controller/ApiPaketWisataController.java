package com.example.GemilangCarRental.controller;

import com.example.GemilangCarRental.models.Kota;
import com.example.GemilangCarRental.models.PaketWisata;
import com.example.GemilangCarRental.repository.RepoPaketWisata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ApiPaketWisataController {

    @Autowired
    private RepoPaketWisata repoPaketWisata;

    @GetMapping("/getAllPaketWisata")
    public ResponseEntity<List<PaketWisata>> GetAllPaketWisata()
    {
        try {
            List<PaketWisata> paketWisata = this.repoPaketWisata.FindAllPaketWisataOrderByASC();
            return new ResponseEntity<>(paketWisata, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/getByIDPaketWisata/{id}")
    public ResponseEntity<List<PaketWisata>>GetPaketWisataById(@PathVariable("id") Long id)
    {
        try {
            Optional<PaketWisata> paketWisata = this.repoPaketWisata.findById(id);

            if(paketWisata.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(paketWisata, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchpaketwisata/{keyword}")
    public ResponseEntity<List<PaketWisata>> SearchPaketWisataName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<PaketWisata> paketWisata = this.repoPaketWisata.SearchPaketWisata(keyword);
            return new ResponseEntity<>(paketWisata, HttpStatus.OK);
        } else {
            List<PaketWisata> paketWisata = this.repoPaketWisata.findAll();
            return new ResponseEntity<>(paketWisata, HttpStatus.OK);
        }
    }

    @GetMapping("/paketwisatamapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<PaketWisata> paketWisata = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<PaketWisata> pageTuts;

            pageTuts = repoPaketWisata.FindAllPaketWisataOrderByASC(pagingSort);

            paketWisata = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("paketwisata", paketWisata);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addPaketWisata")
    public ResponseEntity<Object> SavePaketWisata(@RequestBody PaketWisata paketWisata)
    {
        try {
            paketWisata.setCreatedBy("Syafiq");
            paketWisata.setCreatedOn(new Date());
            this.repoPaketWisata.save(paketWisata);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editPaketWisata/{id}")
    public ResponseEntity<Object> UpdatePaketWisata(@RequestBody PaketWisata paketWisata, @PathVariable("id") Long id)
    {
        Optional<PaketWisata> paketWisataData = this.repoPaketWisata.findById(id);

        if (paketWisataData.isPresent()){
            paketWisata.setModifiedBy("Syafiq");
            paketWisata.setModifiedOn(new Date());
            paketWisata.setId(id);
            this.repoPaketWisata.save(paketWisata);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletePaketWisata/{id}")
    public ResponseEntity<Object> deletePaketWisata(@PathVariable("id") Long id)
    {
        this.repoPaketWisata.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
