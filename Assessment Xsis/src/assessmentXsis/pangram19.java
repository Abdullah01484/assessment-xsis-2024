package assessmentXsis;

import java.util.Scanner;

public class pangram19 {

    private static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    public static void nomor19() {

        Scanner input = new Scanner(System.in);

        char[] charAlphabetArray = alphabet.toCharArray();

        System.out.print("Masukkan kalimat =  ");
        String text = input.nextLine().toLowerCase();

        int hitung = 0;

        for (int i = 0; i < charAlphabetArray.length; i++) {
            if (text.contains(alphabet.substring(i, i + 1))){
                hitung++;
            }
        }

        if(hitung == 26){
            System.out.println("Pangrams");
        }else{
            System.out.println("Bukan Pangrams");
        }
    }
}
