package assessmentXsis;

import java.util.Arrays;
import java.util.Scanner;

public class mengurutkanAngka12 {

    public static void nomor12(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan angka yang akan di cari diurutkan = ");
        String text = input.nextLine();

        String[] textArray = text.split( " ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);

        }
        Arrays.sort(intArray);

        int nilai = 0;
        int[] hasil = new int[intArray.length];

//      mencari nilai urutan
        for (int i = 0; i < intArray.length; i++) {
            for (int j = 0; j < intArray.length; j++) {
                if (intArray[i] > intArray[j]){
                    nilai ++;
                }
            }
            hasil[i] = nilai;
            nilai = 0;
        }

//      mengurutkan nilai urutan
        for (int i = 0; i < intArray.length; i++) {
            for (int j = 0; j < intArray.length; j++) {
                if (i == hasil[j]){
                    System.out.print(intArray[j] + " ");
                }
            }

        }
        System.out.println();
    }

}
