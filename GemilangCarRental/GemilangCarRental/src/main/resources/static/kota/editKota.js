$(document).ready(function() {
	getKotaById();

})

var createdOn;
var createdBy;

function getKotaById() {
	var id = $("#editKotaId").val();
	$.ajax({
		url: "/api/getByIDKota/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_by;
            createdOn = data.created_on;
			$("#kotaInput").val(data.namaKota);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idkota = $("#editKotaId").val();
	var namakota = $("#kotaInput").val();

	if (namakota == "") {
		$("#errKota").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKota").text("");
	}

	var obj = {};
	obj.created_by = createdBy;
	obj.id = idkota;
	obj.namaKota = namakota;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editKota/" + idkota,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllKota();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
