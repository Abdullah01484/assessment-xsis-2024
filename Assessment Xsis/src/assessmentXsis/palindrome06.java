package assessmentXsis;

import java.util.Scanner;

public class palindrome06 {

    public static void nomor06() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan kata/angka : ");
        String angka = input.nextLine();

        char[] inputanChar = angka.toCharArray();

        char[] inputanCharBalik = new char[inputanChar.length];

        int count = 1;
        for (int i = 0; i < inputanChar.length; i++) {
            inputanCharBalik[i] = inputanChar[inputanChar.length-count];
            count ++;
        }

        int helper = 0;
        for (int i = 0; i < inputanChar.length; i++) {
            if (inputanChar[i] == inputanCharBalik[i]) {
                helper ++;
            }
        }
        if (helper == inputanChar.length) {
            System.out.println("Kata/Angka Termasuk Palindrome");
        } else {
            System.out.println("Kata/Angka Tidak Termasuk Palindrome");
        }
    }

}
