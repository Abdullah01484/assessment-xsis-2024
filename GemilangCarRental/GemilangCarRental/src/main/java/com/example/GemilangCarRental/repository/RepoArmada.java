package com.example.GemilangCarRental.repository;

import com.example.GemilangCarRental.models.Armada;
import com.example.GemilangCarRental.models.Kota;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoArmada extends JpaRepository<Armada, Long> {

    @Query(value = "SELECT * FROM armada ORDER BY id ASC", nativeQuery = true)
    List<Armada> FindAllArmadaOrderByASC();

    @Query(value = "SELECT * FROM armada ORDER BY armada ASC", nativeQuery = true)
    Page<Armada> FindAllArmadaOrderByASC(Pageable pageable);

    @Query(value = "SELECT * FROM armada WHERE lower(Kendaraan) LIKE lower(concat('%',?1,'%') ) ", nativeQuery = true)
    List<Armada> SearchArmada(String keyword);

}
