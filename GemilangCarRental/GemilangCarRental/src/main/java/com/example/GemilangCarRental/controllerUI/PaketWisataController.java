package com.example.GemilangCarRental.controllerUI;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("paket_wisata")
public class PaketWisataController {

    @RequestMapping("")
    public String paketWisata(){ return ("paketwisata/paketwisata"); }

    @RequestMapping("addPaketWisata")
    public String addPaketWisata(){ return "paketwisata/addPaketWisata";}

    @RequestMapping("editPaketWisata/{id}")
    public String editPaketWisata(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "paketwisata/editPaketWisata";
    }
    @RequestMapping("deletePaketWisata/{id}")
    public String deletePaketWisata(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "paketwisata/deletePaketWisata";
    }

}
