package assessmentXsis;

import java.util.Scanner;

public class awalAkhir10 {

    public static void nomor10(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nama: ");
        String inputNama = input.nextLine();

        String[] textStringArray = inputNama.split(" ");

        System.out.println("Output = ");
        for (int i = 0; i < textStringArray.length; i++) {
            char[] textCharArray = textStringArray[i].toCharArray();
            for (int j = 0; j < textCharArray.length; j++) {
                if (j == 0){
                    System.out.print(textCharArray[j] + "***");
                }
                else if (j == textCharArray.length - 1){
                    System.out.print(textCharArray[j] + " ");
                }
            }
        }
        System.out.println();

    }
}
