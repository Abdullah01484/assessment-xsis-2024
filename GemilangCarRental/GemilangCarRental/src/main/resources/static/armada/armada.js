function getAllArmada(){
	$("#armadaTable").html(
		`<thead>
			<tr>
				<th>IdArmada</th>
				<th>Kendaraan</th>
				<th>Harga</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="armadaTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getAllArmada",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#armadaTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].kendaraan}</td>
						<td>${data[i].harga}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editArmada(this.value)" class="btn btn-warning">
                        	<i class="bi-pencil-square"> Edit</i>
                        	</button>
                        	<button value="${data[i].id}" onClick="deleteArmada(this.value)" class="btn btn-danger">
                            <i class="bi-trash"> Delete</i>
                            </button>
                        <td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/armada/addArmada",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Armada");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editArmada(id){
	$.ajax({
		url: "/armada/editArmada/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Armada");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteArmada(id){
	$.ajax({
		url: "/armada/deleteArmada/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Armada");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function ArmadaList(currentPage, length) {
    $.ajax({
		url : '/api/armadamapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="ArmadaList (0,this.value)">'
			table += '<option value="...">...</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>IDArmada</th> <th>Kendaraan</th> <th>Harga</th> <th>Action</th></tr>"
			for (let i = 0; i < data.armada.length; i++) {
				table += "<tr>";
				table += "<td>" + data.armada[i].id + "</td>";
				table += "<td>" + data.armada[i].kendaraan + "</td>";
				table += "<td>" + data.armada[i].harga + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.armada[i].id + "' onclick=editArmada(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.armada[i].id + "' onclick=deleteArmada(this.value)>Delete</button></td>";
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="ArmadaList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="ArmadaList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="ArmadaList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#armadaList').html(table);
		}

	});
}

function SearchArmada(request) {
    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searcharmada/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>IDArmada</th> <th>Kendaraan</th> <th>Harga</th><th>Action</th></tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
                        table += "<td>" + result[i].id + "</td>";
                        table += "<td>" + result[i].kendaraan + "</td>";
                        table += "<td>" + result[i].harga + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editArmada(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteArmada(this.value)>Delete</button></td>";
						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='4' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#armadaList').html(table);
			}
		});
	} else {
        ArmadaList(0,5);
	}
}

$(document).ready(function(){
//	getAllArmada();
    ArmadaList(0,5);
});