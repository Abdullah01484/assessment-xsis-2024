package com.example.GemilangCarRental.controller;

import com.example.GemilangCarRental.models.Armada;
import com.example.GemilangCarRental.models.Kota;
import com.example.GemilangCarRental.repository.RepoKota;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ApiKotaController {

    @Autowired
    private RepoKota repoKota;

    @GetMapping("/getAllKota")
    public ResponseEntity<List<Kota>> GetAllKota()
    {
        try {
            List<Kota> kota = this.repoKota.FindAllKotaOrderByASC();
            return new ResponseEntity<>(kota, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/getByIDKota/{id}")
    public ResponseEntity<List<Kota>>GetKotaById(@PathVariable("id") Long id)
    {
        try {
            Optional<Kota> kota = this.repoKota.findById(id);

            if(kota.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(kota, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchkota/{keyword}")
    public ResponseEntity<List<Kota>> SearchKotaName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Kota> kota = this.repoKota.SearchKota(keyword);
            return new ResponseEntity<>(kota, HttpStatus.OK);
        } else {
            List<Kota> kota = this.repoKota.findAll();
            return new ResponseEntity<>(kota, HttpStatus.OK);
        }
    }

    @GetMapping("/kotamapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Kota> kota = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Kota> pageTuts;

            pageTuts = repoKota.FindAllKotaOrderByASC(pagingSort);

            kota = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("kota", kota);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addKota")
    public ResponseEntity<Object> SaveKota(@RequestBody Kota kota)
    {
        try {
            kota.setCreatedBy("Syafiq");
            kota.setCreatedOn(new Date());
            this.repoKota.save(kota);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editKota/{id}")
    public ResponseEntity<Object> UpdateKota(@RequestBody Kota kota, @PathVariable("id") Long id)
    {
        Optional<Kota> kotaData = this.repoKota.findById(id);

        if (kotaData.isPresent()){
            kota.setModifiedBy("Syafiq");
            kota.setModifiedOn(new Date());
            kota.setId(id);
            this.repoKota.save(kota);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deleteKota/{id}")
    public ResponseEntity<Object> deleteKota(@PathVariable("id") Long id)
    {
        this.repoKota.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
