package com.example.GemilangCarRental.models;

import jakarta.persistence.*;

@Entity
@Table(name = "armada")
public class Armada extends CommenEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name= "kendaraan", nullable = false)
    private String Kendaraan;

    @Column(name = "harga", nullable = false)
    private String Harga;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getKendaraan() {
        return Kendaraan;
    }

    public void setKendaraan(String kendaraan) {
        Kendaraan = kendaraan;
    }

    public String getHarga() {
        return Harga;
    }

    public void setHarga(String harga) {
        Harga = harga;
    }
}
