$("#addBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$(document).ready(function(){
   pilihanOpsi();
})
function pilihanOpsi(){
    var harga = document.getElementById("opsiKota");

    $.ajax({
            url : "/api/getAllKota",
            type : "GET",
            contentType : "application/json",
            success : function(data){
                for(i = 0; i < data.length; i++){
                    harga.options[harga.options.length] = new Option(data[i].namaKota, data[i].id);
                }
            }
    });
}

$("#addBtnCreate").click(function(){
	var kota = $("#opsiKota").val();
	var wisata = $("#namaWisata").val();
	var kapasitas = $("#namaKapasitas").val();
	var harga = $("#namaHarga").val();

	if(kota == ""){
    		$("#errKota").text("Name tidak boleh kosong!");
    		return;
    } else {
    		$("#errKota").text("");
    }
	if(wisata == ""){
		$("#artNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#artNama").text("");
	}
	if(kapasitas == ""){
    	$("#arrKapasitas").text("Initial tidak boleh kosong!");
    	return;
    } else {
    	$("#arrKapasitas").text("");
    }
    if(harga == ""){
    	$("#arrHarga").text("Initial tidak boleh kosong!");
    	return;
    } else {
    	$("#arrHarga").text("");
    }

	var obj = {};
	obj.namaWisata = wisata;
	obj.kotaId = kota;
	obj.kapasitas = kapasitas;
	obj.harga = harga;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addPaketWisata",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})