$(document).ready(function() {
	getPengemudiById();

})

var createdOn;
var createdBy;

function getPengemudiById() {
	var id = $("#delPengemudiId").val();
	$.ajax({
		url: "/api/getByIDPengemudi/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idDelete").text(data.namaPengemudi);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delPengemudiId").val();
	$.ajax({
		url : "/api/deletePengemudi/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})