package assessmentXsis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class perpustakaan02 {

    public static void nomor02() {
        Scanner input = new Scanner(System.in);


        DateFormat df = new SimpleDateFormat("dd MMMM yyyy", new Locale("id"));
        String[][] bukuPinjaman = {{"A","B","C","D"},{"14","3","7","7"}};

        System.out.println("Masukkan Tanggal Peminjaman dan Pengembalian");
        System.out.println("Contoh Format input : 28 Februari 2016 - 7 Maret 2016");
        String inputTanggal = input.nextLine();

        Date tanggalPinjam = null;
        Date tanggalPengembalian = null;
        boolean flag = true;

        while (flag){
            String[] splitTanggal = inputTanggal.split(" - ");
            try {
                tanggalPinjam = df.parse(splitTanggal[0]);
                tanggalPengembalian = df.parse(splitTanggal[1]);
                flag = false;
            } catch (ParseException e) {
                System.out.println("Format tidak sesuai!");
                System.out.println("Masukkan lagi tanggal peminjaman sampai pengembalian");
                inputTanggal = input.nextLine();
            }
        }

        long selisih = (tanggalPengembalian.getTime() - tanggalPinjam.getTime());
        int hari = (int) ((selisih / 1000) / 60 / 60 / 24);

        System.out.println("Output");
        for (int i = 0; i < bukuPinjaman[0].length; i++) {
            String namaBuku = bukuPinjaman[0][i];
            int durasiPeminjaman = Integer.parseInt(bukuPinjaman[1][i]);
            if (hari > durasiPeminjaman){
                int denda = (hari - durasiPeminjaman) * 100;
                System.out.print("Buku " + namaBuku + " Denda = " + denda);
            }else {
                System.out.print("Buku " + namaBuku + " Denda = " + 0);
            }
            System.out.println();
        }

    }
}
