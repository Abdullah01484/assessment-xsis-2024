package com.example.GemilangCarRental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GemilangCarRentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(GemilangCarRentalApplication.class, args);
	}

}
