$(document).ready(function() {
	getArmadaById();
})

var createdOn;
var createdBy;

function getArmadaById() {
	var id = $("#delArmadaId").val();
	$.ajax({
		url: "/api/getByIDArmada/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#idDelete").text(data.kendaraan);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delArmadaId").val();
	$.ajax({
		url : "/api/deleteArmada/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllArmada();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})