package com.example.GemilangCarRental.repository;

import com.example.GemilangCarRental.models.Armada;
import com.example.GemilangCarRental.models.Pengemudi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoPengemudi extends JpaRepository<Pengemudi, Long> {

    @Query(value = "SELECT * FROM pengemudi ORDER BY id ASC", nativeQuery = true)
    List<Pengemudi> FindAllPengemudiOrderByASC();

    @Query(value = "SELECT * FROM pengemudi ORDER BY pengemudi ASC", nativeQuery = true)
    Page<Pengemudi> FindAllPengemudiOrderByASC(Pageable pageable);

    @Query(value = "SELECT * FROM pengemudi WHERE lower(nama_pengemudi) LIKE lower(concat('%',?1,'%') ) ", nativeQuery = true)
    List<Pengemudi> SearchPengemudi(String keyword);

}
