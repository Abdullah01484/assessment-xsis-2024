package assessmentXsis;

import java.util.Arrays;
import java.util.Scanner;

public class meanMedianModus07 {

    public static void nomor07(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka = ");
        String deret = input.nextLine();

        String[] textArray = deret.split( " ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);

        }
        Arrays.sort(intArray);
        double median = 0;
        double mean = 0;
        double modus = 0;
        double jumlah = 0;

        for (int i = 0; i < intArray.length; i++) {
            if (intArray.length % 2 != 0 ) {
                median= intArray[intArray.length/2];
            }else{
                median = (double) (intArray[(intArray.length/2) - 1] + intArray[(intArray.length/2)]) / 2;
            }
        }

        for (int i = 0; i < intArray.length; i++) {
            jumlah += intArray[i];
            mean = jumlah / textArray.length;
        }

        int[] nilai = new int[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            nilai[i] = 0;
        }
        for (int i = 0; i < intArray.length; i++) {
            nilai[intArray[i]] ++;
        }

        for (int i = 0; i < intArray.length; i++) {
            if (nilai[i] > 0 && i < modus) {
                modus = i;
            }
        }

        System.out.println("Median = " + median);
        System.out.println("Mean = " + mean);
        System.out.println("Modus = " + modus);
    }

}
