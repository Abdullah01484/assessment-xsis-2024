package com.example.GemilangCarRental.controller;

import com.example.GemilangCarRental.models.Armada;
import com.example.GemilangCarRental.models.Kota;
import com.example.GemilangCarRental.repository.RepoArmada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ApiArmadaController {

    @Autowired
    private RepoArmada repoArmada;

    @GetMapping("/getAllArmada")
    public ResponseEntity<List<Armada>> GetAllArmada()
    {
        try {
            List<Armada> armada = this.repoArmada.FindAllArmadaOrderByASC();
            return new ResponseEntity<>(armada, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/getByIDArmada/{id}")
    public ResponseEntity<List<Armada>>GetArmadaById(@PathVariable("id") Long id)
    {
        try {
            Optional<Armada> armada = this.repoArmada.findById(id);

            if(armada.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(armada, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searcharmada/{keyword}")
    public ResponseEntity<List<Armada>> SearchArmadaName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Armada> armada = this.repoArmada.SearchArmada(keyword);
            return new ResponseEntity<>(armada, HttpStatus.OK);
        } else {
            List<Armada> armada = this.repoArmada.findAll();
            return new ResponseEntity<>(armada, HttpStatus.OK);
        }
    }

    @GetMapping("/armadamapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Armada> armada = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Armada> pageTuts;

            pageTuts = repoArmada.FindAllArmadaOrderByASC(pagingSort);

            armada = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("armada", armada);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addArmada")
    public ResponseEntity<Object> SaveArmada(@RequestBody Armada armada)
    {
        try {
            armada.setCreatedBy("Syafiq");
            armada.setCreatedOn(new Date());
            this.repoArmada.save(armada);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editArmada/{id}")
    public ResponseEntity<Object> UpdateArmada(@RequestBody Armada armada, @PathVariable("id") Long id)
    {
        Optional<Armada> armadaData = this.repoArmada.findById(id);

        if (armadaData.isPresent()){
            armada.setModifiedBy("Syafiq");
            armada.setModifiedOn(new Date());
            armada.setId(id);
            this.repoArmada.save(armada);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deleteArmada/{id}")
    public ResponseEntity<Object> deleteArmada(@PathVariable("id") Long id)
    {
        this.repoArmada.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
