$(document).ready(function() {
	getPengemudiById();

})

var createdOn;
var createdBy;

function getPengemudiById() {
	var id = $("#editPengemudiId").val();
	$.ajax({
		url: "/api/getByIDPengemudi/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_by;
            createdOn = data.created_on;
			$("#nomorInput").val(data.noPegawai);
			$("#namaInput").val(data.namaPengemudi);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idpengemudi = $("#editPengemudiId").val();
	var pegawai = $("#nomorInput").val();
	var pengemudi = $("#namaInput").val();

	if (pegawai == "") {
		$("#errNom").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNom").text("");
	}
	if (pengemudi == "") {
    	$("#errNama").text("Name tidak boleh kosong!");
    	return;
    } else {
    	$("#errNama").text("");
    }

	var obj = {};
	obj.created_by = createdBy;
	obj.id = idpengemudi;
	obj.noPegawai = pegawai;
	obj.namaPengemudi = pengemudi;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editPengemudi/" + idpengemudi,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
