package com.example.GemilangCarRental.models;

import jakarta.persistence.*;

@Entity
@Table(name = "paket_wisata")
public class PaketWisata extends CommenEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "nama_wisata", nullable = false)
    private String namaWisata;

    @ManyToOne
    @JoinColumn(name = "kota", insertable = false, updatable = false)
    public Kota kota;

    @Column(name = "kota")
    private long kotaId;

    @Column(name = "kapasitas")
    private String kapasitas;

    @Column(name = "harga")
    private String harga;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public Kota getKota() {
        return kota;
    }

    public void setKota(Kota kota) {
        this.kota = kota;
    }

    public long getKotaId() {
        return kotaId;
    }

    public void setKotaId(long kotaId) {
        this.kotaId = kotaId;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
