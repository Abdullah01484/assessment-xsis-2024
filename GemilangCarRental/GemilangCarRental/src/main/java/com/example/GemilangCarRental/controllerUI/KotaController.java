package com.example.GemilangCarRental.controllerUI;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("kota")
public class KotaController {

    @RequestMapping("")
    public String kota(){ return ("kota/kota"); }

    @RequestMapping("addKota")
    public String addkota(){ return "kota/addKota";}

    @RequestMapping("editKota/{id}")
    public String editKota(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "kota/editkota";
    }
    @RequestMapping("deleteKota/{id}")
    public String deleteKota(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "kota/deletekota";
    }

}
