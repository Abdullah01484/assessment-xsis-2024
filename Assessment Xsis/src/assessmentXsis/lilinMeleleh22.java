package assessmentXsis;

import java.util.Scanner;

public class lilinMeleleh22 {

    public static void nomor22() {
        Scanner input = new Scanner(System.in);

        System.out.println("Contoh input : 3 3 9 6 7 8 23");
        System.out.print("Masukkan Tinggi Lilin : ");
        String text = input.nextLine().trim();

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray [i] = Integer.parseInt(textArray[i]);
        }

        int helper1 = 0;
        int helper2 = 1;

        int[] deretFibo = new int[intArray.length];

        for (int i = 0; i < intArray.length; i++) {

            if (i == 0) {
                deretFibo[i] = helper2;
            }
            else{
                deretFibo[i] = helper1 + helper2;
                helper1 = helper2;
                helper2 = deretFibo[i];
            }
        }

        System.out.print("Bilangan Fibonacci          : ");

        for (int i = 0; i < deretFibo.length; i++) {
            if (i == deretFibo.length - 1)
            {
                System.out.print(deretFibo[i]);
            }
            else {
                System.out.print(deretFibo[i] + " ");
            }
        }

        int counter = 0;
        int[] counterArray = new int[intArray.length];

        for (int i = 0; i < intArray.length; i++) {
            while (intArray[i] > 0){
                intArray[i] -= deretFibo[i];
                counter++;
            }
            counterArray[i] = counter;
            counter = 0;
        }
        System.out.println();
        System.out.print("Berapa detik lilin habis    : ");
        for (int i = 0; i < counterArray.length; i++) {
            System.out.print(counterArray[i] + " ");
        }

        int helperCounter = counterArray[0];

        for (int i = 0; i < intArray.length; i++) {
            if (counterArray[i] < helperCounter ){
                helperCounter = counterArray[i];
            }
        }

        System.out.println();

        System.out.print("Lilin yang pertama habis adalah lilik ke - ");
        for (int i = 0; i < intArray.length; i++) {
            if (counterArray[i] == helperCounter){
                System.out.print(i + 1);
            }
        }
        System.out.println();
    }
}
