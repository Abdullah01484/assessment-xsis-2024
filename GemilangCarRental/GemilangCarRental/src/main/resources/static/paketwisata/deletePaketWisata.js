$(document).ready(function() {
	getPaketWisataById();

})

function getPaketWisataById() {
	var id = $("#delPaketWisataId").val();
	$.ajax({
		url: "/api/getByIDPaketWisata/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idDelete").text(data.namaWisata);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delPaketWisataId").val();
	$.ajax({
		url : "/api/deletePaketWisata/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})