package assessmentXsis;

import java.util.Scanner;

import static assessmentXsis.kalimatDibalik11.nomor11;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        boolean flag = true;
        String answer = "y";

        while (flag){
            System.out.println("Masukkan nomor soal 1-22 :");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 22){
                System.out.println("nomor tidak tersedia");
                pilihan = input.nextInt();
            }

            switch (pilihan){
            case 1:
                membeliBarang01.nomor01();
                break;
            case 2:
                perpustakaan02.nomor02();
                break;
            case 3:
                tarifParkir03.nomor03();
                 break;
            case 4:
                bilanganPrima04.nomor04();
                break;
            case 5:
                fibonacci05.nomor05();
                break;
            case 6:
                palindrome06.nomor06();
                break;
            case 7:
                meanMedianModus07.nomor07();
                break;
            case 8:
                minMax08.nomor08();
                break;
            case 9:
                kaliLipat09.nomor09();
                break;
            case 10:
                awalAkhir10.nomor10();
                break;
            case 11:
                kalimatDibalik11.nomor11();
                break;
            case 12:
                mengurutkanAngka12.nomor12();
                break;
            case 13:
                sudutBerdasarkanJam13.nomor13();
                break;
            case 14:
                rotasi14.nomor14();
                break;
            case 15:
                merubahWaktu15.nomor15();
                break;
            case 16:
                makanIkan16.nomor16();
                break;
            case 17:
                ninjaHattori17.nomor17();
                break;
            case 18:
                olahraga18.nomor18();
                break;
            case 19:
                pangram19.nomor19();
                break;
            case 20:
                batuGuntingKertas20.nomor20();
                break;
            case 21:
                walkJump21.nomor21();
                break;
            case 22:
                lilinMeleleh22.nomor22();
                break;
                default:
            }
            System.out.println("Try again? y/n");
            input.nextLine();
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
            flag = false;
            }
        }

    }
}