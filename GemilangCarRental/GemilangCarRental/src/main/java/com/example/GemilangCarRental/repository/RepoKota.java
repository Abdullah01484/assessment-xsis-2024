package com.example.GemilangCarRental.repository;

import com.example.GemilangCarRental.models.Kota;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepoKota extends JpaRepository<Kota, Long> {

    @Query(value = "SELECT * FROM kota ORDER BY id ASC", nativeQuery = true)
    List<Kota> FindAllKotaOrderByASC();

    @Query(value = "SELECT * FROM kota ORDER BY kota ASC", nativeQuery = true)
    Page<Kota> FindAllKotaOrderByASC(Pageable pageable);

    @Query(value = "SELECT * FROM kota WHERE lower(nama_kota) LIKE lower(concat('%',?1,'%') ) ", nativeQuery = true)
    List<Kota> SearchKota(String keyword);
}
