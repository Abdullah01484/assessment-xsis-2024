package assessmentXsis;

import java.util.Scanner;

public class fibonacci05 {

    public static void nomor05() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nilai n = ");
        int n = input.nextInt();

        int[] hasil = new int[n];
        hasil[0] = 1;
        hasil[1] = 1;

        for (int i = 2; i < n; i++) {
            hasil[i] = hasil[i - 1] + hasil[i - 2];
        }

        for (int i = 0; i < n; i++) {
            System.out.print(hasil[i] + " ");
        }
        System.out.println();
    }
}
