package com.example.GemilangCarRental.controllerUI;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("pengemudi")
public class PengemudiController {

    @RequestMapping("")
    public String pengemudi(){ return ("pengemudi/pengemudi"); }

    @RequestMapping("addPengemudi")
    public String addpengemudi(){ return "pengemudi/addPengemudi";}

    @RequestMapping("editPengemudi/{id}")
    public String editPengemudi(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "pengemudi/editpengemudi";
    }
    @RequestMapping("deletePengemudi/{id}")
    public String deletePengemudi(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "pengemudi/deletepengemudi";
    }

}
