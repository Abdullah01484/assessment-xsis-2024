package assessmentXsis;

import java.util.Scanner;

public class bilanganPrima04 {

    public static void nomor04() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nilai n = ");
        int n = input.nextInt();

        int prima = 0;
        int counter = 0;
        int [] results = new int[n];

        for (int i = 0; i < n * 10; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0){
                    prima++;
                }
            }
            if (prima == 2){
                results[counter] = i;
                counter++;
            }
            if (counter == n){
                break;
            }
            prima = 0;
        }
        System.out.print("Output = ");
        for (int k = 0; k < n; k++) {
            System.out.print(results[k] + " ");
        }
        System.out.println();
    }
}
