package assessmentXsis;

import java.util.Scanner;

public class rotasi14 {

    public static void nomor14(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Angka Rotasi = ");
        String angka = input.nextLine();
        System.out.print("Rotasi Mulai Dari Baris Ke = ");
        int n = input.nextInt();

        String[] angkaArray =angka.split(" ");

        for (int i = 0; i < n; i++) {
            String temp = angkaArray[0];
            for (int j = 0; j < angkaArray.length - 1; j++) {
                angkaArray[j] = angkaArray[j + 1];
            }
            angkaArray[angkaArray.length - 1] = temp;
        }
        System.out.print("cetak hasil rotasi = ");
        for (int k = 0; k < angkaArray.length; k++) {
            System.out.print(angkaArray[k] + " ");
        }
        System.out.println();
    }

}
