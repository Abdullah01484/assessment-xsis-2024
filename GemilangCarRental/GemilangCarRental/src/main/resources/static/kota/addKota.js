$("#editCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#editCreateBtn").click(function(){
	var kota = $("#kotaInput").val();

	if(kota == ""){
		$("#errKota").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKota").text("");
	}

	var obj = {};
	obj.namaKota = kota;

	var myJson = JSON.stringify(obj);

	$.ajax({
	    url : "/api/addKota",
	    type : "POST",
	    contentType : "application/json",
	    data : myJson,
	    success: function(data){
	    		$(".modal").modal("hide")
	    		location.reload();
	    },
	    error: function(){
	    	alert("Terjadi kesalahan")
	    }
	});
})
