function getAllKota(){
	$("#kotaTable").html(
		`<thead>
			<tr>
				<th>IdKota</th>
				<th>Kota</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="kotaTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getAllKota",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#kotaTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].namaKota}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editKota(this.value)" class="btn btn-warning">
                        	<i class="bi-pencil-square"> Edit</i>
                        	</button>
                        	<button value="${data[i].id}" onClick="deleteKota(this.value)" class="btn btn-danger">
                            <i class="bi-trash"> Delete</i>
                            </button>
                        <td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/kota/addKota",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Kota");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editKota(id){
	$.ajax({
		url: "/kota/editKota/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Kota");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteKota(id){
	$.ajax({
		url: "/kota/deleteKota/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Kota");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function KotaList(currentPage, length) {
    $.ajax({
		url : '/api/kotamapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="KotaList(0,this.value)">'
			table += '<option value="...">...</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>IDKota</th> <th>Kota</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.kota.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.kota[i].namaKota + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.kota[i].id + "' onclick=editKota(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.kota[i].id + "' onclick=deleteKota(this.value)>Delete</button></td>";
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="KotaList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="KotaList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="KotaList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#kotaList').html(table);
		}

	});
}

function SearchKota(request) {

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchkota/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>IDKota</th> <th>Kota</th> <th>Action</th> </tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
                        table += "<td>" + result[i].id + "</td>";
                        table += "<td>" + result[i].namaKota + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editKota(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteKota(this.value)>Delete</button></td>";
						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='3' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#kotaList').html(table);
			}
		});
	} else {
        KotaList(0,5);
	}
}

$(document).ready(function(){
//    getAllKota();
    KotaList(0,5);
})