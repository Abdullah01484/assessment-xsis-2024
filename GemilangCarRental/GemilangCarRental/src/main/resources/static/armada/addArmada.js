$("#editCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#editCreateBtn").click(function(){
	var jenismobil = $("#kenInput").val();
	var jumlah = $("#harInput").val();

	if(jenismobil == ""){
		$("#errKen").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKen").text("");
	}
	if(jumlah == ""){
    	$("#errHar").text("Initial tidak boleh kosong!");
    		return;
    } else {
    	$("#errHar").text("");
    }

	var obj = {};
	obj.kendaraan = jenismobil;
	obj.harga = jumlah;

	var myJson = JSON.stringify(obj);

	$.ajax({
	    url : "/api/addArmada",
	    type : "POST",
	    contentType : "application/json",
	    data : myJson,
	    success: function(data){
	    		$(".modal").modal("hide")
	    		location.reload();
	    },
	    error: function(){
	    	alert("Terjadi kesalahan")
	    }
	});

})