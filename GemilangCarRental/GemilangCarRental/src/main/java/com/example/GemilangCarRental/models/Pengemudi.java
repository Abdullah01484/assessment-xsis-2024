package com.example.GemilangCarRental.models;

import jakarta.persistence.*;

@Entity
@Table(name = "pengemudi")
public class Pengemudi extends CommenEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name= "no_pegawai", nullable = false)
    private String noPegawai;

    @Column(name = "nama_pengemudi",nullable = false)
    private String namaPengemudi;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNoPegawai() {
        return noPegawai;
    }

    public void setNoPegawai(String noPegawai) {
        this.noPegawai = noPegawai;
    }

    public String getNamaPengemudi() {
        return namaPengemudi;
    }

    public void setNamaPengemudi(String namaPengemudi) {
        this.namaPengemudi = namaPengemudi;
    }
}
